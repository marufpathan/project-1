import { Component, OnInit } from '@angular/core';
import { Item } from '../products/products.component';
import { WishlistService } from '../wishlist.service';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';


@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css'],
})
export class WishlistComponent implements OnInit {
  items: Item[] = [];

  constructor(
    private cartService: CartService,
    private wishlistService: WishlistService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.items = this.wishlistService.getWishlistItems();
  }

  addToCart(item: Item) {
    this.cartService.addToCart(item);
  }

  removeFromWishlist(item: Item) {
    this.wishlistService.removeFromWishlist(item);
    this.items = this.wishlistService.getWishlistItems(); // Update the wishlist items after removal
  }
}