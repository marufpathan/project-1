import { Component, OnInit } from '@angular/core';

import { Route, Router } from '@angular/router';
import { CustomCurrencyPipe } from '../currency.pipe';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [CustomCurrencyPipe],
})
export class HeaderComponent implements OnInit {
  selectedCurrency = 'USD';

  id = '';

  constructor(public cartService: CartService, private router: Router) {}

  ngOnInit() {}

  setCurrency(currency: string) {
    this.selectedCurrency = currency;
  }

  drop(value: any) {
    if (this.id === value) {
      this.id = '';
    } else {
      this.id = value;
    }
  }

  goToCheckout() {
    this.router.navigate(['/checkout']);
  }
  goToAbout() {
    this.router.navigate(['/about']);
  }
  goToWishlist() {
    this.router.navigate(['/wishlist']);
  }
}
