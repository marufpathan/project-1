import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  emailTrue:any = false;
  phoneTrue:any=false;
  selectionTrue:any=true;
  emailOTP:any=false;
  otp:any;
  tempNO: any;
  fullotp:any='';
  @ViewChild('firstInput') firstInput: any;
  @ViewChild('secondInput') secondInput: any;
  @ViewChild('thirdInput') thirdInput: any;
  @ViewChild('fourthInput') fourthInput: any;
  @ViewChild('fifthInput') fifthInput: any;
  @ViewChild('sixthInput') sixthInput: any;
  intOTP: number | undefined;
  resetPass: boolean=false;
  credentials: any;
  isEmail:any=false;
  isPhone:any=false;



  constructor(private router: Router,
    private fb: FormBuilder,
    private service: UserService){

  

  }

  changePass(forgotPass:any){
    
  }

  onClickEmail(){
    this.selectionTrue=false;
    this.emailTrue=true;
 

  }

  onClickPhone(){
    this.selectionTrue=false;
    this.phoneTrue=true;
    
  }

  sendOTPtoMail(forgotPass:any){
    console.log('inside email otp');
    console.log(forgotPass);
    this.service.sendOTPtoMail(forgotPass.emailId).subscribe((response:any)=>{
      console.log('sent succesfully');
      this.isEmail=true;
      this.credentials=forgotPass.emailId;
      this.tempNO= response;
      this.emailTrue=false;
      this.emailOTP= true;
    })
  }

  sendOTPtoPhone(forgotPass:any){
    console.log('inside phone otp');
    console.log(forgotPass);
    this.service.sendOTPtoPhone(forgotPass.phoneNum).subscribe((response:any)=>{
      console.log('sent succesfully');
      this.isPhone=true;
      this.credentials=forgotPass.phoneNum;
      this.tempNO= response;
      this.phoneTrue=false;
      this.emailOTP= true;
    })
  }

  updatePassWithPhone(recoveryForm:any){
    console.log('inside updatepasswithpno');
    console.log(recoveryForm.password);
    console.log(this.credentials);
    this.service.updatePassWithPhone(this.credentials,recoveryForm.password).subscribe((data:any)=>{
        if(data!=null){
          this.router.navigate(['/login']);
        }
    })

  }

  updatePassWithMail(recoveryForm:any){
    console.log('inside updatepasswithemail');
    console.log(recoveryForm.password);
    console.log(this.credentials);
    this.service.updatePassWithMail(this.credentials,recoveryForm.password).subscribe((data:any)=>{
      if(data!=null){
        this.router.navigate(['/login']);
      }
  })
  }

  checkOTP(otpForm:any){
    console.log(this.tempNO);
    console.log(otpForm);
    this.fullotp += (otpForm.first);
    this.fullotp+=(otpForm.second);
    this.fullotp+=(otpForm.third);
    this.fullotp+=(otpForm.fourth);
    this.fullotp+=(otpForm.fifth);
    this.fullotp+=(otpForm.sixth);
    console.log(this.fullotp);
    this.intOTP = parseInt(this.fullotp);
    if(this.intOTP === this.tempNO){
      console.log('otp matched');
      // this.router.navigate(['/login']);
      this.emailOTP=false;
      this.resetPass=true;
    }else{
      console.log('otp did not match');
    }
  }


  onKeyUp(event: any, nextInput: any) {
    const input = event.target as HTMLInputElement;
    const inputValue = input.value;

    if (inputValue.length === 1 && nextInput) {
      nextInput.focus();
    }
  }


}
