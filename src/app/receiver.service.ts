import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReceiverService {

  private receiverSource = new BehaviorSubject<any>(null);
  receiverData = this.receiverSource.asObservable();

  constructor() { }

  setReceiver(receiver: any) {
    this.receiverSource.next(receiver);
  }

  
}
