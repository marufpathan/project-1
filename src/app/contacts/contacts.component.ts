import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewContainerRef,
} from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ChatComponent } from '../chat/chat.component';
import { ReceiverService } from '../receiver.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
})
export class ContactsComponent implements OnInit {
  contacts: any;
  user: any;
  friendIds: any;
  userDetailsPromises: any;
  searchUser: any;
  // Component class
  imageUrl: string | undefined;
  showModal: boolean = false;

  constructor(
    private receiverService: ReceiverService,
    private service: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.user = this.service.getUserData();
    this.openContacts();
  }

  openChat(contact: any) {
    this.receiverService.setReceiver(contact);
    console.log(contact);
    this.router.navigate(['/home/chat']);
  }

  openContacts() {
    console.log(this.user.userId);

    this.service.getFriends(this.user.userId).subscribe((friends: any) => {
      this.friendIds = friends.map((friend: any) => friend.friendUser.userId);
      this.userDetailsPromises = this.friendIds.map((friendId: any) =>
        this.service.getProfileInfo(friendId)
      );

      forkJoin<any[]>(this.userDetailsPromises).subscribe(
        (userDetails: any[]) => {
          console.log('User Details:', userDetails); // Log the user details array

          this.contacts = friends.map((friend: any, index: number) => ({
            friendUser: friend.friendUser,
            userDetails: userDetails[index],
          }));

          console.log('Contacts:', this.contacts); // Log the final contacts array
        }
      );
    });
  }

  onSearch(SearchKey: any) {
    this.service.searchUsers(SearchKey).subscribe((user) => {
      console.log('searchusers : ');
      this.searchUser = user;
      console.log(this.searchUser);
    });
  }

  // Inside your component class
  closeModal() {
    this.searchUser = null; // Set searchUser to null to hide the modal
  }

  SendFriendRequest(receiverId: any) {
    this.service.addFriend(this.user.userId, receiverId).subscribe(() => {
      this.openContacts();
    });
  }

  removefriend(receiverId: any) {
    this.service.removeFriend(this.user.userId, receiverId).subscribe(() => {
      this.openContacts();
    });
  }

  openModal(imageUrl: string): void {
    console.log('inside openModal');
    this.imageUrl = imageUrl;
    console.log(this.imageUrl);
    this.showModal = true;
  }
}
