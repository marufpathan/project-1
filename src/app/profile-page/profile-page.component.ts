import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})

export class ProfilePageComponent implements OnInit {
  userInfo:any ;
  profileInfo: any;
  imageUrl: string | undefined;
  tempbob!: Blob;
  updateUserDetails:any;
  

  constructor(private service: UserService, private router : Router){

    this.profileInfo ={ userImage:'',about :'' , userId:' '};
  }


  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      const base64String = e.target?.result as string;
      if (base64String) {
        this.profileInfo.userImage = base64String;
        this.imageUrl = base64String;
      }
    };
    reader.readAsDataURL(file);
  }
  

  ngOnInit(): void {
    this.userInfo= this.service.getUserData();
    console.log(this.userInfo);
   
    this.service.getProfileInfo(this.userInfo.userId).subscribe((infodata:any)=>{
      this.profileInfo = infodata;
      console.log('profileInfo is : ');
      console.log(this.profileInfo);
        this.imageUrl = this.profileInfo.userImage;
    }) ;

  }

  updateData(){
    console.log('profile data is printing ');
    console.log(this.profileInfo);
    this.service.updateDetailsUser(this.profileInfo).subscribe((data :any)=>{
    
      
    });
    this.service.setProfileImage(this.profileInfo.userImage);
    console.log('user data is printing ');
    console.log(this.userInfo);
    this.service.updateUser(this.userInfo).subscribe((data :any)=>{
      // this.userInfo= this.service.getUserData();
      
      this.service.setUserData(this.userInfo);
     
    }) ;

     this.router.navigate(["/home/profile"]);
    //  this.router.navigate(['/home']);

  }

    
}
