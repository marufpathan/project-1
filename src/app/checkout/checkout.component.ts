import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';
import { Item } from '../products/products.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



declare var paypal: any;
interface CartItem {
  item: Item;
  quantity: number;
}

declare global {
  interface Window {
    paypal: any;
  }
}

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  items: Item[] = [];
  total: number = 0;
  cart: CartItem[] = [];
  selectedCurrency: string = 'INR';

  constructor(private router: Router,private cartService: CartService , ) {}

  ngOnInit() {
    this.items = this.cartService.getItems();
    console.log(this.items);
    this.cart = this.cartService.getCart();
    console.log(this.cart)
    this.updateTotal();
    console.log(this.updateTotal())
    // this.loadPayPalSDK()
    //   .then(() => {})
    //   .catch((error) => {
    //     console.error('Error loading PayPal SDK:', error);
    //   });
  }
  clearCart() {
    this.cartService.clearCart();
    this.items = this.cartService.getItems();
    this.cart = this.cartService.getCart();
    this.updateTotal();
  }

  getCartTotal(): number {
    let total = 0;
    this.cart.forEach((cartItem: CartItem) => {
      total += cartItem.item.price * cartItem.quantity;
    });
    return total;
  }

  updateTotal() {
    const vat = 0.05;
    const totalAmount = this.getCartTotal() * (1 + vat);
    this.total = totalAmount;
  }

  removeFromCart(item: Item) {
    this.cartService.removeFromCart(item);
    this.cart = this.cartService.getCart();
    this.updateTotal();
  }

  addToCart(item: Item) {
    this.cartService.addToCart(item);
    this.cart = this.cartService.getCart();
    this.updateTotal();
  }

  incrementQuantity(index: number) {
    const cartItem = this.cart[index];
    cartItem.quantity++;
    this.updateTotal();
  }

  decrementQuantity(index: number) {
    const cartItem = this.cart[index];
    if (cartItem.quantity > 1) {
      cartItem.quantity--;
      this.updateTotal();
    }
  }

  loadPayPalSDK(): Promise<void> {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src =
        'https://www.paypal.com/sdk/js?client-id=ASLRbWlUdOyy9NTZ2ty_tvyrqjudqBJ0q0kmlOWgnnlAkDJuX55PeBYFK7jUqw7ogxcAczIT27BmKecD';
      scriptElement.onload = () => {
        resolve();
      };
      scriptElement.onerror = () => {
        reject('Failed to load PayPal SDK');
      };
      document.body.appendChild(scriptElement);
    });
  }
  handlePaypalCheckout() {
    if (!window.paypal) {
      console.error('PayPal SDK not loaded');
      return;
    }

    const createOrder = () => {
      return window.paypal
        .request({
          method: 'POST',
          url: '/your-server-endpoint-to-create-order',
          headers: {
            'Content-Type': 'application/json',
          },
          body: {
            amount: this.total.toFixed(2),
            currency: this.selectedCurrency,
          },
        })
        .then((response: { id: any }) => {
          return response.id;
        });
    };

    createOrder().then((orderId: string) => {
      window.paypal
        .Buttons({
          createOrder: createOrder,
          onApprove: async (
            data: any,
            actions: { order: { capture: () => any } }
          ) => {
            const details = await actions.order.capture();

            this.saveTransaction(orderId, details);

            this.router.navigate(['/success']);
          },
          onError: (error: any) => {
            console.error('PayPal error:', error);
          },
        })
        .render('#paypal-button-container');
    });
  }

  goToPayment(){
    this.cartService.setTotal(this.total);
    this.router.navigate(['home/header/payment']);
  }

  saveTransaction(orderId: string, details:any){}
}
