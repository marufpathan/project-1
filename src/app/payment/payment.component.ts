import { Component, HostListener } from '@angular/core';
import { OrderService } from '../order.service';
import { UserService } from '../user.service';
import { CartService } from '../cart.service';
import { HttpClient } from '@angular/common/http';

declare var Razorpay: any; // Add this line to declare the Razorpay variable

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent {
  title = 'demo';
  total:any;
  paymentObj:any;
  userData:any;
  constructor(private http: HttpClient,
    private orderService:OrderService , private service: UserService , private cartService : CartService) {
      this.paymentObj = { name: '', email: '', phone: '', amount: ''  }
  }

  ngOnInit() {
    
    // this.service.getUserData().subscribe((data:any)=>{
    //   this.userData=data;
    // });
    this.total= this.cartService.getTotal();
    console.log(this.total)
    this.userData = this.service.getUserData();
    console.log(this.userData);
    this.paymentObj.name= this.userData.username;
    this.paymentObj.email= this.userData.email;
    this.paymentObj.phone= this.userData.phoneNum;
    this.paymentObj.amount= this.total;

    this.onSubmit();
  }

  sayHello() {
    alert("Hello DK");
  }

  paymentId!: string;
  error!: string;
  
  options = {
    "key": "",
    "amount": "", 
    "name": "LinkUp",
    "description": "Web Development",
    "image": "",
    "order_id":"",
    "handler": function (response: any){
      var event = new CustomEvent("payment.success", 
        {
          detail: response,
          bubbles: true,
          cancelable: true
        }
      );	  
      window.dispatchEvent(event);
    }
    ,
    "prefill": {
    "name": "",
    "email": "",
    "contact": ""
    },
    "notes": {
    "address": ""
    },
    "theme": {
    "color": "#3399cc"
    }
    };
  
    onSubmit(): void {
      console.log('inside on submit method ')
      this.paymentId = ''; 
      this.error = ''; 
      this.orderService.createOrder(this.paymentObj).subscribe(
        (      data: { secretId: string; razorpayOrderId: string; applicationFee: string; pgName: string; }) => {
        this.options.key = data.secretId;
        this.options.order_id = data.razorpayOrderId;
        this.options.amount = data.applicationFee; //paise
        this.options.prefill.name = "LinkUp";
        this.options.prefill.email = "linkupofficial227@gmail.com";
        this.options.prefill.contact = "9999999999";
        
        if(data.pgName ==='razor2') {
          this.options.image="";
          var rzp1 = new Razorpay(this.options);
          rzp1.open();
        } else {
          var rzp2 = new Razorpay(this.options);
          rzp2.open();
        }
       
                
        rzp1.on('payment.failed',  (response: { error: { code: any; description: any; source: any; step: any; reason: any; metadata: { order_id: any; payment_id: any; }; }; }) =>{    
          // Todo - store this information in the server
          console.log(response);
          console.log(response.error.code);    
          console.log(response.error.description);    
          console.log(response.error.source);    
          console.log(response.error.step);    
          console.log(response.error.reason);    
          console.log(response.error.metadata.order_id);    
          console.log(response.error.metadata.payment_id);
          this.error = response.error.reason;
        }
        );
      }
      ,
        (      err: { error: { message: string; }; }) => {
        this.error = err.error.message;
      }
      );
    }

    @HostListener('window:payment.success', ['$event']) 
    onPaymentSuccess(event: { detail: any; }): void {
       console.log(event.detail);
    }
}