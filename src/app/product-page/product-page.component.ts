import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../products/item';

import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product-service.service';
import { CartService } from '../cart.service';
@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css'],
})
export class ProductPageComponent implements OnInit {
  // @Input() item!: Item;
  largeImage!: string;
  showLarge!: boolean;
  id:any;
  iteam:any;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    // const id = +this.route.snapshot.params['id'];
    this.id = this.productService.getIteamId();
    this.iteam = this.productService.getProduct(this.id);

    // if (this.iteam === undefined) {
    //   console.error('Product not found');
    // } else {
    //   this.item = item;
    // }
  }

  addToCart(item: Item) {
    console.log('Add to cart:', item);
  }

  goToBack() {
    this.router.navigate(['/products']);
  }
  imageChange(event: MouseEvent) {
    console.log('Image changed:', event);
  }
  addToWishlist(item: Item) {
    console.log('Add to wishlist:', item);
  }
  showLargeImage(imgUrl: string) {
    this.largeImage = imgUrl;
    this.showLarge = true;
  }
}
