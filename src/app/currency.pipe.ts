import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'customCurrency',
})
export class CustomCurrencyPipe implements PipeTransform {
  transform(
    value: any,
    currencyCode: string = 'INR',  // set default currency code to INR
    symbolDisplay: string = 'symbol',
    digits: string = '1.2-2'
  ): any {
    const parsedValue = parseFloat(value);
    if (isNaN(parsedValue)) {
      return '';
    }

    const currencyPipe = new CurrencyPipe('en-US');
    const formattedValue = currencyPipe.transform(
      parsedValue,
      currencyCode,
      symbolDisplay,
      digits
    );

    if (formattedValue == null) {
      return '';
    }

    if (currencyCode === 'INR') {
      return formattedValue.replace('$', '₹');
    } else {
      return formattedValue;
    }
  }
}

export { CurrencyPipe };
