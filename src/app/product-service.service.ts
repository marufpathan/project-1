import { Injectable } from '@angular/core';
import { Item } from './products/products.component';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private iteamId:any;
  
  private items: Item[] = [
    {
      id: 1,
      name: 'Item 1',
      price: 10.99,
      discount: 2,
      label: 'Sale',
      imgUrl: '../assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 2,
      name: 'Item 2',
      price: 15.99,
      discount: 2,
      label: 'Sale',
      imgUrl: '../assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 3,
      name: 'Item 3',
      price: 8.99,
      discount: 2,
      label: 'Sale',
      imgUrl: '../assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 4,
      name: 'Item 4',
      price: 12.99,
      discount: 1,
      imgUrl: '../assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 5,
      name: 'Item 5',
      price: 10.99,
      discount: 2,
      label: 'Sale',
      imgUrl: './assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 6,
      name: 'Item 6',
      price: 15.99,
      discount: 2,
      label: 'Sale',
      imgUrl: './assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 7,
      name: 'Item 7',
      price: 8.99,
      discount: 2,
      label: 'Sale',
      imgUrl: './assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
    {
      id: 8,
      name: 'Item 8',
      price: 12.99,
      discount: 1,
      imgUrl: './assets/images/Batman Tshirt.jpg',
      quantity: 1,
    },
  ];

  getProducts(): Item[] {
    return this.items;
  }

  getProduct(id: number): Item | undefined {
    return this.items.find((item) => item.id === id);
  }

  setIteamId(id: any) {
    this.iteamId = id;
  }

  getIteamId() {
    return this.iteamId;
  }

}



// import { Injectable } from '@angular/core';
// import { Item } from './products/products.component';

// @Injectable({
//   providedIn: 'root',
// })
// export class ProductService {
//   private iteamId:any;
  
//   private items: Item[] = [
//     {
//       id: 1,
//       name: 'Item 1',
//       price: 10.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: '../assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 2,
//       name: 'Item 2',
//       price: 15.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: '../assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 3,
//       name: 'Item 3',
//       price: 8.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: '../assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 4,
//       name: 'Item 4',
//       price: 12.99,
//       discount: 1,
//       imgUrl: '../assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 5,
//       name: 'Item 5',
//       price: 10.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: './assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 6,
//       name: 'Item 6',
//       price: 15.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: './assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 7,
//       name: 'Item 7',
//       price: 8.99,
//       discount: 2,
//       label: 'Sale',
//       imgUrl: './assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//     {
//       id: 8,
//       name: 'Item 8',
//       price: 12.99,
//       discount: 1,
//       imgUrl: './assets/images/Batman Tshirt.jpg',
//       quantity: 1,
//     },
//   ];

//   getProducts(): Item[] {
//     return this.items;
//   }

//   getProduct(id: number): Item | undefined {
//     return this.items.find((item) => item.id === id);
//   }

//   setIteamId(id: any) {
//     this.iteamId = id;
//   }

//   getIteamId() {
//     return this.iteamId;
//   }

// }
