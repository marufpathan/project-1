import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { AuthGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { ChatComponent } from './chat/chat.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AboutComponents } from './about/about.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductPageComponent } from './product-page/product-page.component';
import { ProductsComponent } from './products/products.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { HeaderComponent } from './header/header.component';
import { PaymentComponent } from './payment/payment.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PostingPageComponent } from './posting-page/posting-page.component';

// const routes: Routes = [
//   {
//     path: 'home',
//     component: HomeComponent,
//     // canActivate: [AuthGuard],
//     children: [
//       { path: '', redirectTo: 'chat', pathMatch: 'full' },
//       { path: 'chat', component: ChatComponent },
//       { path: 'contacts', component: ContactsComponent },
//       {path:'profile',component: ProfilePageComponent},
//     ],
//   },
//   { path: 'login', component: LoginComponent },
//   { path: 'register', component: RegisterComponent },
//   { path: '', redirectTo: '/login', pathMatch: 'full' },
//   { path: '**', redirectTo: '/login', pathMatch: 'full' },
//   { path: 'products', component: ProductsComponent },
//   { path: 'checkout', component: CheckoutComponent },
//   { path: 'about', component: AboutComponents },
//   { path: 'wishlist', component: WishlistComponent },
//   { path: 'product-page', component: ProductPageComponent },
//   { path: 'product-view/:id', component: ProductPageComponent},
// ];

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    // canActivate: [AuthGuard],
    children: [
      { path:'*', redirectTo: 'contacts', pathMatch: 'full' },
      { path:'', redirectTo: 'contacts', pathMatch: 'full' },
      { path: 'chat', component: ChatComponent },
      { path: 'contacts', component: ContactsComponent },
      { path:'profile',component: ProfilePageComponent },
      { path: 'posting-page', component: PostingPageComponent },
      {
        path: 'header',
        component: HeaderComponent ,
        children: [
          { path: '', redirectTo: 'products', pathMatch: 'full' },
          { path: 'products', component: ProductsComponent },
          { path: 'checkout', component: CheckoutComponent },
          { path: 'about', component: AboutComponents },
          { path: 'wishlist', component: WishlistComponent },
          { path: 'product-page', component: ProductPageComponent },
          { path: 'product-view/:id', component: ProductPageComponent },
          { path: 'payment', component: PaymentComponent },
        ]
      },   
    ],
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path:'forgot-password',component:ForgotPasswordComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/login', pathMatch: 'full' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
