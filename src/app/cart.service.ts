import { Injectable } from '@angular/core';
import { Item } from './products/products.component';

interface CartItem {
  item: Item;
  quantity: number;
}

@Injectable({
  providedIn: 'root',
})
export class CartService {
  
  cart: CartItem[] = [];
  total:any;

  addToCart(item: Item) {
    console.log('inside serice add to cart')
    const existingCartItem = this.cart.find(
      (cartItem) => cartItem.item.id === item.id
    );
    if (existingCartItem) {
      existingCartItem.quantity++;
    } else {
      this.cart.push({ item, quantity: 1 });
    }
    console.log(this.cart);
  }
  
  getCart(): CartItem[] {
    return this.cart;
  }

  getCartTotal(): number {
    let total = 0;
    this.cart.forEach((cartItem) => {
      total += cartItem.item.price * cartItem.quantity;
    });
    return total;
  }

  removeFromCart(item: Item) {
    const index = this.cart.findIndex((cartItem) => cartItem.item === item);
    if (index > -1) {
      this.cart.splice(index, 1);
    }
  }

  getItems(): Item[] {
    return this.cart.map((cartItem) => cartItem.item);
  }

  clearCart() {
    this.cart = [];
  }

  setTotal(total: number) {
    this.total=total;
  }

  getTotal(){
    return this.total;
  }

  constructor(){}
}
