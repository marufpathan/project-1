import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';

export const AUTH_SERVICE = 'AUTH_SERVICE';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user : any;
  private isUserLogged: boolean;
  private loginStatus: Subject<any>;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }


  getCurrentUser(): any {
    const user = localStorage.getItem('currentUser');
    return user ? JSON.parse(user) : null;
  }

}
