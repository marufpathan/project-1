import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'E-Com';

  constructor( private service: UserService ) {}

  isLoggedIn() {
    return this.service.getUserLoggedStatus();
  }
  
}
