import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ReceiverService } from '../receiver.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css'],
})
export class ChatComponent implements OnInit, OnDestroy {
  name: any = 'test';
  message: string = '';
  receiver: any;
  chatsData: any;
  chats: any;
  intervalId: any;
  selectedFile: File | null;
  showModal:any=false;
  showEmojiPicker: boolean = false;
  emojis: string[] = ['😀', '😃', '😄', '😊', '😎', '😍', '😘', '🤩', '🤪', '😜', '😂', '🤣', '😇', '😉', '😋', '😚', '🙃', '🙂', '🤗', '🤔', '🤐', '🤫', '🤨', '😐', '😶', '😏', '🙄', '😷', '🤒', '🤕', '🤢', '🤮', '😱', '😨', '😰', '😭', '😡', '😠', '🤯', '💩'];


  constructor(
    private service: UserService,
    private receiverService: ReceiverService,
    private http: HttpClient
  ) {
    this.selectedFile = null;
  }

  ngOnInit(): void {
    console.log(this.receiver);
    this.receiverService.receiverData.subscribe((receiver) => {
      this.receiver = receiver;
    });
    console.log(this.receiver);

    this.getChatMessages();

    this.intervalId = setInterval(() => {
      this.getChatMessages();
    }, 2000);
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }

  viewProfile(){
    this.showModal=true;
  }

  getChatMessages() {
    this.service
      .getAllMessages(this.service.getUserId(), this.receiver.friendUser.userId)
      .subscribe((data: any) => {
        console.log(data);
        this.chats = data;
      });
  }

  sendMessage(receiverId: any) {
    if (this.selectedFile) {
      const formData = new FormData();
      formData.append('file', this.selectedFile);
      formData.append('message', this.message);

      this.sendWithAttachment(receiverId, formData);
    } else {
      this.sendWithoutAttachment(receiverId);
    }

    this.message = '';
    this.selectedFile = null;
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    console.log(this.selectedFile);

    // Send the file directly without waiting for sendMessage
    if (this.selectedFile) {
      const formData = new FormData();
      formData.append('file', this.selectedFile);

      this.sendWithAttachment(this.receiver.friendUser.userId, formData);
    }
  }

  sendWithoutAttachment(receiverId: any) {
    this.service
      .sendMessage(receiverId, this.service.getUserId(), this.message)
      .subscribe((data: any) => {
        console.log('Message sent without attachment');
        console.log(data);
      });
  }

  sendWithAttachment(receiverId: any, formData: FormData) {
    this.service
      .sendWithAttachment(receiverId, this.service.getUserId(), formData)
      .subscribe((data: any) => {
        console.log('returned data is ');
        console.log(data);
      });
  }

  getAttachmentUrl(attachmentPath: string) {
    // Modify the implementation according to your backend API
    // Return the URL or endpoint to fetch the attachment file
    this.service.downloadFile(attachmentPath).subscribe((data: any) => {
      return data;
    });
    // return attachmentPath;
    // return this.http.get('http://localhost:8080/downloadFile/'+attachmentPath);
  }

  // downloadFile(fileUrl:any){
  //   this.service.downloadFile(fileUrl).subscribe(()=>{
  //   }) ;
  // }
  // downloadFile(fileUrl: string) {
  //   this.service.downloadFile(fileUrl).subscribe(
  //     (response: any) => {
  //       const fileName = this.getFileNameFromResponseHeaders(response);
  //       this.saveFile(response.body, fileName);
  //     },
  //     (error: any) => {
  //       console.error('Error downloading file:', error);
  //     }
  //   );
  // }

  downloadFile(fileName: string) {
    const fileUrl = `http://localhost:8080/downloadFile/${fileName}`;
    this.http.get(fileUrl, { responseType: 'blob' }).subscribe(
      (response: Blob) => {
        this.saveFile(response, fileName);
      },
      (error: any) => {
        console.error('Error downloading file:', error);
      }
    );
  }

  saveFile(response: Blob, fileName: string) {
    const blob = new Blob([response], { type: response.type });
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = fileName;
    link.click();
    window.URL.revokeObjectURL(url);
  }

  toggleEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
  }

  addEmoji(emoji: string) {
    this.message += emoji;
  }



}

//=================================WebSocketCode======================================
// export class ChatComponent implements OnInit, OnDestroy {
//   // Other component properties
//   name: any = 'test';
//   message: string = '';
//   receiver: any;
//   chatsData: any;
//   chats: any;
//   selectedFile: File | null | undefined;

//   private webSocketSubscription: Subscription | undefined;

//   constructor(
//     private userService: UserService,
//     private receiverService: ReceiverService,
//     private webSocketService: WebSocketService
//   ) {
//     // Other constructor logic
//   }

//   ngOnInit(): void {

// this.receiverService.receiverData.subscribe((receiver) => {
//   this.receiver = receiver;
// });
// console.log(this.receiver);

//     // this.getChatMessages();
//     // Connect to the WebSocket server
//     console.log('+++before connection ');
//     this.webSocketService.connect();
//     console.log('+++after connection ');

//     // Subscribe to WebSocket messages
//     this.webSocketSubscription = this.webSocketService
//       .receive(this.userService.getUserId(),this.receiver)
//       .subscribe((data: any) => {
//         // Handle received messages
//         console.log('Received WebSocket message: ', data);

//         this.chats.push(data);
//       });

//     this.getChatMessages(); // Fetch initial chat messages
//   }

//   ngOnDestroy(): void {

//     this.webSocketService.disconnect();
//     this.webSocketSubscription?.unsubscribe();
//   }

//   // Other component methods

//   getChatMessages() {
//     this.userService
//       .getAllMessages(
//         this.userService.getUserId(),
//         this.receiver.friendUser.userId
//       )
//       .subscribe(
//         (data: any) => {
//           console.log('Chat data: ', data);
//           this.chats = data;
//         },
//         (error: any) => {
//           console.error('Failed to fetch chat messages: ', error);
//         }
//       );
//   }

//   sendMessage(receiverId: any) {
//     if (this.selectedFile) {
//       const formData = new FormData();
//       formData.append('file', this.selectedFile);
//       formData.append('message', this.message);

//       this.sendWithAttachment(receiverId, formData);
//     } else {
//       this.sendWithoutAttachment(receiverId);
//     }

//     this.message = '';
//     this.selectedFile = null;
//   }

//   onFileSelected(event: any) {
//     this.selectedFile = event.target.files[0];
//     console.log(this.selectedFile);
//   }

//   sendWithoutAttachment(receiverId: any) {
//     this.userService.sendMessage(receiverId, this.userService.getUserId(), this.message)
//       .subscribe(
//         (data: any) => {
//           console.log('Message sent without attachment');
//           console.log(data);
//         },
//         (error: any) => {
//           console.error('Failed to send message without attachment: ', error);
//         }
//       );
//   }

//   sendWithAttachment(receiverId: any, formData: FormData){
//     this.userService
//       .sendWithAttachment(receiverId, this.userService.getUserId(), formData)
//       .subscribe(
//         (data: any) => {
//           console.log('Message sent with attachment');
//           console.log(data);
//         },
//         (error: any) => {
//           console.error('Failed to send message with attachment: ', error);
//         }
//       );
//   }

//   getAttachmentUrl(attachment: any) {

//   }
// }

//=================================SetINtervalCode======================================

// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
// import { ReceiverService } from '../receiver.service';

// @Component({
//   selector: 'app-chat',
//   templateUrl: './chat.component.html',
//   styleUrls: ['./chat.component.css'],
// })
// export class ChatComponent implements OnInit {
// name: any = 'test';
// message: string = '';
// receiver: any;
// chatsData: any;
// chats: any;
// selectedFile: File | null;

//   constructor(
//     private service: UserService,
//     private receiverService: ReceiverService
//   ) {this.selectedFile = null;}

//   ngOnInit(): void {
//     console.log(this.receiver);
// this.receiverService.receiverData.subscribe((receiver) => {
//   this.receiver = receiver;
// });
// console.log(this.receiver);

// this.getChatMessages();

//     setInterval(() => {
//       this.getChatMessages();
//     }, 1000);
//   }

//   getChatMessages() {
//     this.service
//       .getAllMessages(this.service.getUserId(), this.receiver.friendUser.userId)
//       .subscribe((data: any) => {
//         console.log('checking chat data     ====' + data);
//         this.chats = data;
//       });
//   }

//   sendMessage(receiverId: any) {
//     if (this.selectedFile) {
//       const formData = new FormData();
//       formData.append('file', this.selectedFile);
//       formData.append('message', this.message);

//       this.sendWithAttachment(receiverId, formData);
//     } else {
//       this.sendWithoutAttachment(receiverId);
//     }

//     this.message = '';
//     this.selectedFile = null;
//   }

//   onFileSelected(event: any) {
//     this.selectedFile = event.target.files[0];
//     console.log(this.selectedFile);
//   }

//   sendWithoutAttachment(receiverId: any) {
//     this.service.sendMessage(receiverId, this.service.getUserId(), this.message).subscribe((data: any) => {
//       console.log('Message sent without attachment');
//       console.log(data);
//     });
//   }

//   sendWithAttachment(receiverId: any, formData: FormData) {
//     this.service.sendWithAttachment(receiverId, this.service.getUserId(), formData).subscribe((data:any)=>{
//         console.log('returned data is ');
//         console.log(data);
//     });
//   }

//   getAttachmentUrl(attachment: any) {

//   }

// }
