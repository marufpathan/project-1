import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { Observable, of } from 'rxjs';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  countries: any;
  user:any;
  registerStatus:any;
  registerForm!: FormGroup;
  error = '';
  isSubmitted = false;
  userExist:any = false;
  emailExist:any=false;
  userNameexists: any;
  emailexists: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: UserService,
    private router: Router
  ) {
    this.user={ username: '', email: '', password: '', phoneNum: '', country: '', gender : ''};
  }

  ngOnInit(): void {
    this.service.getAllCountries().subscribe((data: any)=>{
      this.countries=data;
    })

    

    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required,Validators.minLength(8)]],
      gender: new FormControl('', Validators.required),
      phoneNumber: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern('^[0-9]*$')
        ]
      ],
      country: ['', Validators.required]
   });  
  }

  isChecked(gender: string): boolean {
    return this.registerForm.controls['gender'].value === gender;
  }
  
  onGenderChange(gender: string): void {
    this.registerForm.controls['gender'].setValue(gender);
  }
  
  

  onSubmit() {
    
    this.isSubmitted = true;

    this.service.checkUsername(this.registerForm.get('username')?.value).subscribe((existOrNot:any)=>{
      this.userNameexists=existOrNot;
      console.log('email is :'+this.userNameexists)
    })
      
    this.service.checkemailId(this.registerForm.get('email')?.value).subscribe((existOrNot:any)=>{
      this.emailexists=existOrNot;
      console.log('email is :'+this.emailexists)
    })
    
    if(this.userNameexists===null && this.emailexists===null){

    this.user.username = this.registerForm.get('username')?.value;
    this.user.email = this.registerForm.get('email')?.value;
    this.user.password = this.registerForm.get('password')?.value;
    this.user.phoneNum = this.registerForm.get('phoneNumber')?.value;
    this.user.country = this.registerForm.get('country')?.value;
    this.user.gender = this.registerForm.get('gender')?.value;

    // console.log(this.user);

    if(this.registerForm.valid){
      this.service.registerUser(this.user).subscribe((data:any)=>{
        console.log('returned data is :  '+data);
        this.registerStatus = data;
        this.router.navigate(['/login']);
      });
    }
    }
    else if( this.emailexists!=null){
      console.log('email exists')
        this.emailExist=true;
    }

    else{
      console.log('username exists')
      this.userExist= true
    }



  }

}
