import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  setUserLogout() {
    throw new Error('Method not implemented.');
  }

  order: any;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  createOrder(order: { name: any; email: any; phone: any; amount: any }): Observable<any> {
    console.log('amount is : ');
    console.log(order.amount);
    return this.http.post("http://localhost:8080/users/createOrder", {
      userName: order.name,
      emailID: order.email,
      phoneNumber: order.phone,
      amount: order.amount
    }, this.httpOptions);
  }
}

