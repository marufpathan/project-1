import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
 
  newMessage = new Subject<any>();
  isUserLogged: boolean;
  loginStatus: Subject<any>;
  private userDataKey = 'userData';
  ProfileImage:any ;
  userImageKey: any;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  setUserData(user: any): void {
    // this.userData = user;
    localStorage.setItem(this.userDataKey, JSON.stringify(user));
  }

  // setProfileImage(userImage: any) {
  //   console.log('inside setProfileImage');
  //   this.ProfileImage= userImage;
  // }

  setProfileImage(userImage: any) {
    localStorage.setItem(this.userImageKey, userImage);
  }

  // getProfileImage():any {
  //   console.log('inside getProfileImage');
  //   return this.ProfileImage;
  // }

  getProfileImage(): any {
    return localStorage.getItem(this.userImageKey);
  }  

  getUserData(): any {
    // return this.userData;

    const storedUserData = localStorage.getItem(this.userDataKey);
    return storedUserData ? JSON.parse(storedUserData) : null;
  }

  getUser(loginForm: any): any {
    return this.http
      .get(
        'http://localhost:8080/login/' +
          loginForm.email +
          '/' +
          loginForm.password
      )
      .toPromise();
  }

  getUserId(): string | null {
    const userData = this.getUserData();
    return userData ? userData.userId : null;
  }

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  setUserLogout() {
    console.log('user logged out');
    localStorage.removeItem(this.userDataKey);
    this.isUserLogged = false;
  }

  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getFriends(user_Id: any) {
    return this.http.get('http://localhost:8080/friends/' + user_Id);
  }

  registerUser(user: any) {
    return this.http.post('http://localhost:8080/registerUser/', user);
  }

  checkUsername(username: any) {
    return this.http.get('http://localhost:8080/getUserByName/'+ username);
  }

  checkemailId(email: any) {
    return this.http.get('http://localhost:8080/getUserByEmail/'+ email);
  }

  getProfileInfo(userId: any) {
    return this.http.get('http://localhost:8080/getUserDetails/' + userId);
  }

  updateDetailsUser(profileInfo: any) {
    return this.http.put('http://localhost:8080/insertUserDetails/',profileInfo);
  }

  updateUser(userInfos: any): any {
    console.log('inside service updateUser method');
    console.log(userInfos);
    return this.http.put('http://localhost:8080/updateUserInfo', userInfos);
  }

  searchUsers(SearchKey: any) {
    return this.http.get('http://localhost:8080/getUserByEmail/' + SearchKey);
  }

  addFriend(userId: any, receiverId: any) {
    return this.http.post(`http://localhost:8080/addFriends/${userId}/${receiverId}`, null);
  }

  removeFriend(userId: any, receiverId: any) {
    return this.http.delete('http://localhost:8080/deleteFriend/'+userId+'/'+receiverId);
  }

  getAllMessages(userId: any, receiver: any) {
    return this.http.get('http://localhost:8080/findBySenderAndReciever/' + userId + '/' + receiver);
  }


  // sendMessage(receiverId: any, userId: any, message: any) {
  //   const messageData = {
  //     sender: userId,
  //     recipient: receiverId,
  //     messageText: message,
  //   };
  //   return this.http.post('http://localhost:8080/sendMessages', messageData);
  // }

  sendMessage(receiverId: any, userId: any, message: any):any {
    const messageData = {
      sender: userId,
      recipient: receiverId,
      messageText: message,
    };
    console.log(messageData)
    return this.http.post('http://localhost:8080/sendMessages', messageData);
  }


  // sendWithAttachment(receiverId: any, userId: any, formData: FormData):any {
  //   console.log('inside sendWithAttachment method in service');
  //   const data = new FormData();
  //   data.append('userId', userId);
  //   data.append('receiverId', receiverId);
  //   data.append('message', formData.get('message') || '');
  //   data.append('file', formData.get('file') || '');

  //   console.log(data);
  //   return this.http.post('http://localhost:8080/sendWithAttachment', data, { responseType: 'text' });
  // }

  sendWithAttachment(receiverId: any, userId: any, formData: FormData) {
    // Prepare the data to be sent in the request body
    console.log('inside sendWithAttachment method in service');
    const data = new FormData();
    data.append('userId', userId);
    data.append('receiverId', receiverId);
    data.append('message', formData.get('message') || '');
    data.append('file', formData.get('file') || '');
  
    console.log(data);
  
    // Send the HTTP POST request with the data in the request body
    return this.http.post('http://localhost:8080/sendWithAttachment', data, { responseType: 'text' });
  }
  
  
  downloadFile(fileUrl: any) {
    return this.http.get('http://localhost:8080/downloadFile/'+fileUrl);
  }

  updatePassWithMail(email:any , password:any){
    return this.http.post(`http://localhost:8080/updatePasswordEmail/${email}/${password}`,null);
  }

  updatePassWithPhone(phone:any , password:any){

    return this.http.post(`http://localhost:8080/updatePassword/${phone}/${password}`, null);
  }

  sendOTPtoPhone(forgotPass:any){
    return this.http.get('http://localhost:8080/sendsms/+91'+forgotPass);
  }

  sendOTPtoMail(forgotPass:any){
    return this.http.get('http://localhost:8080/sendOTP/'+forgotPass);
  }
}
