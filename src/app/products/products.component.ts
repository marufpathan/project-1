import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from './item';
import { ProductService } from '../product-service.service';
import { WishlistService } from '../wishlist.service'; // Import the WishlistService
import { CartService } from '../cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  showDetails: boolean = false;
  title = 'ngrxdemo';

  wishlist: Item[] = [];
  items: Item[] = [];

  id = '';
  url = '../assets/th.jpg';

  constructor(
    public cartService: CartService,
    private router: Router,
    private productService: ProductService,
    private wishlistService: WishlistService // Add the WishlistService to the constructor
  ) {}

  ngOnInit() {
    this.items = this.productService.getProducts();
  }

  addToWishlist(item: Item) {
    this.wishlistService.addToWishlist(item); // Access the wishlistService
  }

  addToCart({ id, name, price, discount, label, imgUrl, quantity }: Item) {
    console.log('inside add to cart')
    this.cartService.addToCart({
      id,
      name,
      price,
      discount,
      label,
      imgUrl,
      quantity,
    });
    console.log(id, name, price, discount, label, imgUrl, quantity );
  }

  removeFromCart({ id, name, price, discount, label, imgUrl, quantity }: Item) {
    console.log('inside remove to cart')
    this.cartService.removeFromCart({
      id,
      name,
      price,
      discount,
      label,
      imgUrl,
      quantity,
    });
  }

  drop(value: any) {
    this.id = this.id === value ? '' : value;
  }

  imageChange(event: any) {
    this.url = event.target.src;
  }

  getTotal() {
    return this.cartService.getCartTotal();
  }

  goToView() {
    this.router.navigate(['/product-view']);
  }

  goToProduct(id:any){
    console.log('inside goToProduct :'+ id)
    this.productService.setIteamId(id);
    this.router.navigate(['/home/header/product-page']);
  }

}

export{Item};
