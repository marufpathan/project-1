export interface Item {
    id: number;
    name: string;
    price: number;
    discount?: number;
    label?: string;
    imgUrl?: string;
    quantity: number;
  }