import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
// import { profile } from 'console';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit{

  user :any;
  loginError :any;
  profileImage: any;
  profile:any;
 

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private service: UserService
  ) {}

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: [
      '',
      [
        Validators.required
      ],
    ],
  });

  get emailControl() {
    return this.loginForm.get('email');
  }

  get passwordControl() {
    return this.loginForm.get('password');
  }

  async login(loginForm: any) {

    await this.service.getUser(loginForm).then((userData:any)=>{
      this.user= userData;
      console.log(this.user);
      
    });


    if (this.user != null) {
      this.service.setUserLoggedIn();
      this.service.setUserData(this.user);
      // this.service.getProfileInfo(this.user.userId).subscribe((profile:any)=>{
      //   this.service.setProfileImage(profile.userImage);
      // });

      try {
        this.profile = await this.service.getProfileInfo(this.user.userId).toPromise();
        this.service.setProfileImage(this.profile.userImage);
      } catch (error) {
        console.error('Error retrieving profile info:', error);
        // Handle the error appropriately
      }
      this.router.navigate(['/home']);
      
    } else {
      this.loginError ="invalid credentials";
    }
  }

  


  ngOnInit(): void {
    
  }


}
