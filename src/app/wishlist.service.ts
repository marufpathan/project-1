import { Injectable } from '@angular/core';
import { Item } from './products/products.component';

@Injectable({
  providedIn: 'root',
})
export class WishlistService {
  private items: Item[] = [];

  constructor() {}

  addToWishlist(item: Item) {
    const foundItem = this.items.find(
      (wishlistItem) => wishlistItem.id === item.id
    );
    if (!foundItem) {
      this.items.push(item);
    }
  }

  removeFromWishlist(item: Item) {
    const index = this.items.findIndex(
      (wishlistItem) => wishlistItem.id === item.id
    );
    if (index !== -1) {
      this.items.splice(index, 1);
    }
  }

  getWishlistItems(): Item[] {
    return this.items;
  }
}